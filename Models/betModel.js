const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const betsSchema = new Schema({
    date: { type: Date, default: Date.now },
    maxGain : {type : Number  , required: true },
    minGain : {type : Number  , required: true },
    Gr : { type: Number, required : true},
    combo : {type : Number, required : true},
    choices: [],
    selectionCount : {type :Number , required : true},
    miseTotal : {type:Number,required: true},
    stat : {type : Boolean}
});

module.exports = mongoose.model("bet", betsSchema);
