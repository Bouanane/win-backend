const mongoose = require('mongoose');
const express = require("express");

const app = express();


//connect to database
const port = process.env.port || 3000;

mongoose
  .connect(
    "mongodb+srv://Hassen:Hassen_471@bets.mvnca.mongodb.net/<dbname>?retryWrites=true&w=majority",
    { useUnifiedTopology: true, useNewUrlParser: true }
  )
  .then(() => {
    app.listen(port, () => console.log("server on ", { port }));
  })
  .catch((err) => {
    console.log(err);
  });
